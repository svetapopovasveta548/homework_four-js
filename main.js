// 1) Функции нужны для то чтобы написать один раз код поместить его в функцию и потом переиспользовать его в разных частях кода


// 2)Аргумент это то что передается функции при ее вызове. Это может быть просто строка либо переменная, так же можем передать функцию


// 3)Функцию может вернуть нам результат. Например обьявляем функцию и в теле прописываем то что должно нам вернуть функция в конечном итоге при ее вызове


let numberOne = +prompt("Enter your number here");
let numberTwo = +prompt("Enter your number here");
let sign = prompt("+,-,*,/");


while (!numberOne || !numberTwo) {
    if (!numberOne) {
        numberOne = +prompt("Enter your Number again!");
    } else if (!numberTwo) {
        numberTwo = +prompt("Enter your Number again!");
    }
}

function calculator() {
    if (sign == "/") {
        console.log(numberOne / numberTwo);
    } else if (sign == "+") {
        console.log(numberOne + numberTwo);
    } else if (sign == "-") {
        console.log(numberOne - numberTwo);
    } else if (sign == "*") {
        console.log(numberOne * numberTwo);
    }
}

calculator();